
from drawit import Image, Color, Point, PPM, WHITE, BLACK, RED, YELLOW

import math


GRAY = Color(180,180,180)



class Robot(Image):


    def draw_background(self):
        self.draw_rectangle(Point(0, 0), Point(self.width - 1, self.height - 1), WHITE)


    def draw_antenna(self):
        point0 = Point(35*self.width/72, self.height/12)
        point1 = Point(37*self.width/72, 2*self.height/12)
        self.draw_rectangle(point0, point1, BLACK)
        
        center = Point(self.width/2, self.height/12)
        radius = self.width/50
        self.draw_circle(center, radius, RED)
    def draw_head(self):
        point0 = Point(4*self.width/12, 2*self.height/12)
        point1 = Point(8*self.width/12, 4*self.height/12)
        self.draw_rectangle(point0, point1, GRAY)
    
        point2 = Point(5*self.width/12, 6*self.height/24)
        point3 = Point(7*self.width/12, 7*self.height/24)
        self.draw_rectangle(point2, point3, YELLOW)
        
        center1 = Point(11*self.width/24, 5*self.height/24)
        center2 = Point(13*self.width/24, 5*self.height/24)
        radius = self.width/50
        
        self.draw_circle(center1, radius, RED)
        self.draw_circle(center2, radius, RED)
    
    
    def draw_neck(self):
        point0 = Point(5*self.width/12, 4*self.height/12)
        point1 = Point(7*self.width/12, 5*self.height/12)
        self.draw_rectangle(point0, point1, GRAY)   
    def draw_torso(self):
        point0 = Point(3*self.width/12, 5*self.height/12)
        point1 = Point(9*self.width/12, 8*self.height/12)
        self.draw_rectangle(point0, point1, GRAY)  
    def draw_arms(self):
        point0 = Point(2*self.width/12, 6*self.height/12)
        point1 = Point(3*self.width/12, 7*self.height/12)
        self.draw_rectangle(point0, point1, GRAY)
        
        point2 = Point(3*self.width/24, self.height/2)
        point3 = Point(2*self.width/12, 9*self.height/12)
        self.draw_rectangle(point2, point3, GRAY)
        
        point4 = Point(9*self.width/12, 6*self.height/12)
        point5 = Point(10*self.width/12, 7*self.height/12)
        self.draw_rectangle(point4, point5, GRAY)
        
        point6 = Point(10*self.width/12, self.height/2)
        point7 = Point(21*self.width/24, 9*self.height/12)
        self.draw_rectangle(point6, point7, GRAY)
        
    def draw_legs(self):
        point0 = Point(4*self.width/12, 8*self.height/12)
        point1 = Point(5*self.width/12, 11*self.height/12)
        self.draw_rectangle(point0, point1, GRAY)
        
        point2 = Point(7*self.width/12, 8*self.height/12)
        point3 = Point(8*self.width/12, 11*self.height/12)
        self.draw_rectangle(point2, point3, GRAY)
        
if __name__ == '__main__':
    draw_bot = Robot()
    draw_bot.draw_background()
    draw_bot.draw_antenna()
    draw_bot.draw_head()
    draw_bot.draw_neck()
    draw_bot.draw_torso()
    draw_bot.draw_arms()
    draw_bot.draw_legs()
    
    

    PPM.write(draw_bot)    