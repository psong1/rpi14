''' image.py: DrawIt Image '''

from color import Color
from point import Point

import math

class Image(object):
    DEFAULT_WIDTH  = 640
    DEFAULT_HEIGHT = 480

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        ''' Initialize Image object's instance variables '''
        self.width = width
        self.height = height
        self.pixels = []

        for row in range(height):
            pixelrow = []
            for column in range(width):
                pixelrow.append(Color())
            self.pixels.append(pixelrow)

    def __eq__(self, other):
        ''' Returns True if current instance and other object are equal '''
        if self.width == other.width and self.height == other.height:
            return True
        else:
            return False

    def __str__(self):
        ''' Returns string representing Image object '''
        return "Image(width={},height={})".format(self.width, self.height)

    def __getitem__(self, point):
        ''' Returns pixel specified by point

        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        if point.y in range (self.height) and point.x in range (self.width):
            return self.pixels[point.y][point.x]
        else:
            raise IndexError

    def __setitem__(self, point, color):
        ''' Sets pixel specified by point to given color

        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        if point.y in range (self.height) and point.x in range (self.width):
            self.pixels[point.y][point.x] = color
        else:
            raise IndexError

    def clear(self):
        ''' Clears Image by setting all pixels to default Color '''
        for row in range (self.height):
            for column in range (self.width):
                self.pixels[row][column] = Color()
                

    def draw_line(self, point0, point1, color):
        ''' Draw a line from point0 to point1 '''
        xmin = min(point0.x,point1.x)
        xmax = max(point0.x,point1.x)
        ymin = min(point0.y,point1.y)
        ymax = max(point0.y,point1.y)
        a = math.atan2(ymax-ymin, xmax-xmin)
        distance = point0.distance_from(point1)
        if a >= math.pi:
            a = a - math.pi

        d = 0
        while (d <= distance):
            P = Point(point0.x + d*math.cos(a), point0.y + d*math.sin(a))
            self.pixels[int(P.y)][int(P.x)] = color
            d += 0.05           
        
        
    def draw_rectangle(self, point0, point1, color):
        ''' Draw a rectangle from point0 to point1 '''
        xmin = min(point0.x,point1.x)
        xmax = max(point0.x,point1.x)
        ymin = min(point0.y,point1.y)
        ymax = max(point0.y,point1.y)
        for row in range(ymin, ymax):
            for column in range(xmin,xmax):
                self.pixels[row][column] = color
                


    def draw_circle(self, center, radius, color):
        ''' Draw a circle with center point and radius '''

        for column in range(center.x-radius, center.x+radius+1):
            for row in range(center.y-radius, center.y+radius+1):
                point = Point(column,row)
                if (point.distance_from(center) <= radius):
                    self.pixels[row][column] = color


# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
