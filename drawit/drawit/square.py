def draw_square(self, point, length, color):
    ''' Draw a rectangle from point0 to point1 '''

    for row in range(point.y, point.y+length):
        for column in range(point.x, point.x+length):
            self.pixels[row][column] = color
