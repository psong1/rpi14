from drawit import Image, Point, PPM, RED, GREEN, BLUE, MAGENTA, YELLOW, BLACK

import math

class Kiss(Image):

    def draw_face(self):
        w = self.width
        h = self.height

        center = Point(w / 2, h / 2)
        radius = min(w, h) / 2 - 1

        self.draw_circle(center, radius, YELLOW)

    def draw_eyes(self):
        w = self.width / 8
        h = self.height / 8

        center = Point(3.25 * w, 3.5 * h)
        radius = min(w, h) / 2 - 1

        self.draw_circle(center, radius, BLACK)

        center = Point(3.25 * w, 3.75 * h)
        radius = min(w, h) / 2 - 1

        self.draw_circle(center, radius, BLACK)

        point0 = Point(5.5 * w, 3.75 * h)
        point1 = Point(5.35 * w, 3.6 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(5.35 * w, 3.6 * h)
        point1 = Point(5.2 * w, 3.45 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(5.2 * w, 3.45 * h)
        point1 = Point(4.9 * w, 3.3 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.9 * w, 3.3 * h)
        point1 = Point(4.75 * w, 3.45 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.75 * w, 3.45 * h)
        point1 = Point(4.6 * w, 3.6 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.6 * w, 3.6 * h)
        point1 = Point(4.45 * w, 3.75 * h)
        self.draw_rectangle(point1, point0, BLACK)

    def draw_lips(self):
        w = self.width / 8
        h = self.height / 8

        point0 = Point(4 * w, 5 * h)
        point1 = Point(4.15 * w, 5.15 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.15 * w, 5.15 * h)
        point1 = Point(4.3 * w, 5.3 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.3 * w, 5.3 * h)
        point1 = Point(4.45 * w, 5.45 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.45 * w, 5.45 * h)
        point1 = Point(4.3 * w, 5.6 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.3 * w, 5.6 * h)
        point1 = Point(4.15 * w, 5.75 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.15 * w, 5.75 * h)
        point1 = Point(4 * w, 5.9 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.15 * w, 5.9 * h)
        point1 = Point(4.3 * w, 6.05* h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.3 * w, 6.05 * h)
        point1 = Point(4.45 * w, 6.3 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.45 * w, 6.3 * h)
        point1 = Point(4.3 * w, 6.45 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.3 * w, 6.45 * h)
        point1 = Point(4.15 * w, 6.6 * h)
        self.draw_rectangle(point1, point0, BLACK)

        point0 = Point(4.15 * w, 6.6 * h)
        point1 = Point(4 * w, 6.75 * h)
        self.draw_rectangle(point1, point0, BLACK)

    def draw_heart(self):
        w = self.width / 8
        h = self.height / 8

        point0 = Point(5.46 * w, 5.25 * h)
        point1 = Point(5.7 * w, 5.4 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(6.2 * w, 5.25 * h)
        point1 = Point(6.44 * w, 5.4 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5.35 * w, 5.4 * h)
        point1 = Point(5.85 * w, 5.55 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(6.05 * w, 5.4 * h)
        point1 = Point(6.55 * w, 5.55 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5.25 * w, 5.55 * h)
        point1 = Point(5.9 * w, 5.7 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(6 * w, 5.55 * h)
        point1 = Point(6.65 * w, 5.7 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5.15 * w, 5.7 * h)
        point1 = Point(6.75 * w, 6.15 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5.25 * w, 6.15 * h)
        point1 = Point(6.65 * w, 6.3 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5.35 * w, 6.3 * h)
        point1 = Point(6.55 * w, 6.45 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5.5 * w, 6.45 * h)
        point1 = Point(6.4 * w, 6.6 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5.65 * w, 6.6 * h)
        point1 = Point(6.25 * w, 6.75 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5.75 * w, 6.75 * h)
        point1 = Point(6.15 * w, 6.9 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5.85 * w, 6.9 * h)
        point1 = Point(6 * w, 7.05 * h)
        self.draw_rectangle(point1, point0, RED)

        point0 = Point(5. * w, 6.3 * h)
        point1 = Point(5. * w, 6.45 * h)
        self.draw_rectangle(point1, point0, RED)




if __name__ == '__main__':
    kiss = Kiss()
    kiss.draw_face()
    kiss.draw_eyes()
    kiss.draw_lips()
    kiss.draw_heart()

    PPM.write(kiss)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
