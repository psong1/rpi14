Project 01 - Grading
====================

**Group Score**: 27 / 27

**Carly Score**:  3 / 3
...
**Andrew Score**:  3 / 3
...
**Patricia Score**:  3 / 3

Deductions
----------

Comments
--------
Looks great guys, nicely done!

Also nice job on the images, keeping the points relative to the image size. Keep up the good work!