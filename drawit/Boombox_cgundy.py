
#!/usr/bin/env python

from drawit import Image, Color, Point, PPM, WHITE, BLACK, MAGENTA, CYAN

import math

#Boombox Colors
GRAY    = Color(180, 180, 180)


#BoomBox Image Class

class BoomBox(Image):

    def draw_background(self):
        self.draw_rectangle(Point(0, 0), Point(self.width - 1, self.height - 1), WHITE)

    def draw_box(self):
        point0 = Point(7*self.width/8, 7*self.height/8)
        point1 = Point(self.width/8, 5*self.height/16)
        self.draw_rectangle(point0, point1, MAGENTA)

    def draw_handle(self):
        point0 = Point(self.width/4, 5*self.height/16)
        point1 = Point(3*self.width/4, self.height/8)
        self.draw_rectangle(point0, point1, MAGENTA)

        point0 = Point(9*self.width/32, 5*self.height/16)
        point1 = Point(23*self.width/32, 5*self.height/32)
        self.draw_rectangle(point0, point1, WHITE)

    def draw_speakers(self):
        center1 = Point(self.width/4, 11*self.height/16)
        center2 = Point(3*self.width/4, 11*self.height/16)
        speaker_radius = self.height/6
        self.draw_circle(center1, speaker_radius, CYAN)
        self.draw_circle(center2, speaker_radius, CYAN)
        self.draw_circle(center1, 3*speaker_radius/4, BLACK)
        self.draw_circle(center2, 3*speaker_radius/4, BLACK)
        
    def draw_casset_player(self):
        point0 = Point(12*self.width/32, 3*self.height/4)
        point1 = Point(20*self.width/32, 17*self.height/32)
        self.draw_rectangle(point0, point1, CYAN)

        point0 = Point(13*self.width/32, 22*self.height/32)
        point1 = Point(19*self.width/32, 9*self.height/16)
        self.draw_rectangle(point0, point1, BLACK)

        center1 = Point(29*self.width/64, 20*self.height/32)
        center2 = Point(35*self.width/64, 20*self.height/32)
        radius = self.width/32
        self.draw_circle(center1, radius, WHITE)
        self.draw_circle(center2, radius, WHITE)
        self.draw_circle(center1, radius/3, BLACK)
        self.draw_circle(center2, radius/3, BLACK)

    def draw_buttons(self):
        length = self.width/32
        starting_width = 12*self.width/32
        height = 3*self.height/4
        point0 = Point(starting_width, height+length) 
        point1 = Point(starting_width+8*length, height)
        self.draw_rectangle(point0, point1, WHITE)
        point0 = Point(starting_width+1*length, height+length) 
        point1 = Point(starting_width+3*length, height)
        self.draw_rectangle(point0, point1, BLACK)
        point0 = Point(starting_width+5*length, height+length) 
        point1 = Point(starting_width+7*length, height)
        self.draw_rectangle(point0, point1, BLACK)


    def draw_top(self):
        point0 = Point(self.width/6, self.height/2)
        point1 = Point(4*self.width/6, 3*self.height/8)
        self.draw_rectangle(point0, point1, BLACK)
        point0 = Point(3*self.width/16, 15*self.height/32)
        point1 = Point(20*self.width/32,13*self.height/32)
        self.draw_rectangle(point0, point1, GRAY)

        

        center1 = Point(23*self.width/32, 7*self.height/16)
        center2 = Point(26*self.width/32, 7*self.height/16)
        radius = self.width/32
        self.draw_circle(center1, radius, BLACK)
        self.draw_circle(center2, radius, BLACK)
        self.draw_circle(center1, radius/2, WHITE)
        self.draw_circle(center2, radius/2, WHITE)        

        
        

if __name__ == '__main__':
    # Draw Boombox
    boom_box = BoomBox()
    boom_box.draw_background()
    boom_box.draw_box()
    boom_box.draw_handle()
    boom_box.draw_speakers()
    boom_box.draw_casset_player()
    boom_box.draw_buttons()
    boom_box.draw_top()
    

PPM.write(boom_box)




