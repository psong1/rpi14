#!/usr/bin/env python

from drawit import Image, Point, PPM, RED, GREEN, BLUE, WHITE, MAGENTA

import math

class Logo(Image):

#Patti
	def draw_P(self):
		P_row0 = 4.0 * self.height / 8
		P_col0 = self.width / 8
		P_row1 = 7.0 * self.height / 8
		P_col1 = 1.5 * self.width / 8

		self.draw_rectangle(Point(P_col0, P_row0), Point(P_col1, P_row1), RED)

		P_row2 = 4.7 * self.height / 8
		P_col2 = 2.65 * self.width / 8

		self.draw_rectangle(Point(P_col1, P_row0), Point(P_col2, P_row2), RED)

		P_row3 = 5.25 * self.height / 8
		P_col3 = 2.2 * self.width / 8
		P_col4 = 2.8 * self.width / 8

		self.draw_rectangle(Point(P_col3 , P_row2), Point(P_col4 , P_row3), RED)

		P_row4 = 5.95 * self.height / 8

		self.draw_rectangle(Point(P_col1, P_row3), Point(P_col2, P_row4), RED)

	def draw_background(self):
		self.draw_rectangle(Point(0, 0), Point(self.width - 1, self.height - 1), WHITE)
		center = Point(self.width / 2, self.height / 2)
		radius = min(self.width, self.height) / 2 - 1
		self.draw_circle(center, radius, GREEN)

#Carly
	def draw_C(self):
		w = self.width / 16
		h = self.height / 16
		self.draw_rectangle(Point(11 * w, 14 * h), Point(13 * w, 13 * h), MAGENTA)
		self.draw_rectangle(Point(10.5*w, 13*h), Point(11.5*w, 12*h), MAGENTA)
		self.draw_rectangle(Point(12.5*w, 13*h), Point(13.5*w, 12*h), MAGENTA)
		self.draw_rectangle(Point(10*w, 12*h), Point(11*w, 10*h), MAGENTA)
		self.draw_rectangle(Point(12.5*w, 10*h), Point(13.5*w, 9*h), MAGENTA)
		self.draw_rectangle(Point(10.5*w, 10*h), Point(11.5*w, 9*h), MAGENTA)
		self.draw_rectangle(Point(11*w, 9*h), Point(13*w, 8*h), MAGENTA)

#Andrew
	def left_block(self):
		point0 = Point(3*self.width/8,self.height/8)
		point1 = Point(7*self.width/16, 4*self.height/8)
		self.draw_rectangle(point0, point1, BLUE)

	def right_block(self):
		point0 = Point(9*self.width/16,self.height/8)
		point1 = Point(5*self.width/8, 4*self.height/8)
		self.draw_rectangle(point0, point1, BLUE)

	def top_block(self):
		point0 = Point(3*self.width/8,self.height/11)
		point1 = Point(5*self.width/8, self.height/8)
		self.draw_rectangle(point0, point1, BLUE)

	def middle_block(self):
		point0 = Point(7*self.width/16, 3*self.height/16)
		point1 = Point(9*self.width/16, 2*self.height/8)
		self.draw_rectangle(point0, point1, BLUE)

if __name__ == '__main__':
	logo = Logo()
	logo.draw_background()
	logo.draw_C()
	logo.draw_P()
	logo.left_block()
	logo.right_block()
	logo.top_block()
	logo.middle_block()

	PPM.write(logo)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
