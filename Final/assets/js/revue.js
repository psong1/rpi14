/* Globals */
var CurrentNotebook = 'Search';	    /* Current notebook */

/* AJAX functions */

function requestJSON(url, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
    	if (request.readyState == 4 && request.status == 200) {
    	    callback(JSON.parse(request.responseText));
	}
    }

    request.open('GET', url, true);
    request.send();
}

/* Display functions */

function displayTab() {
    var tabs = document.getElementsByClassName('nav-tabs')[0].getElementsByTagName('li');
    for (var i = 0; i < tabs.length; i++) {
        var tab = tabs[i];
        tab.classList.remove('active');
    }
    this.classList.add('active');
    var tabs = this.getElementsByTagName('a')[0].innerHTML;
    switch (tabs) {
        case 'search':
            displaySearch();
            break;
        case 'year':
            displayYear();
            break;
        case 'title':
            displayTitle();
            break;
        case 'skit':
            displaySkit();
            break;
    }
}

function displayResults(url) {
    /* TODO: Request JSON from URL and then call appropriate render function */
    requestJSON(url, function(data) {
        if (data['render'] == 'gallery') {
        renderGallery(data['results'], data['prefix']);
    } else if (data['render'] == 'title') {
        renderTitle(data['results']);
    } else if (data['render'] == 'skit') {
        renderSkit(data['results']);
    }
});
}

function displaySearch(query) {
    CurrentNotebook = 'Search';

    var element   = document.getElementById('notebook-body-nav');
    var renderHTML = '<form class="navbar-form text-center">';

    renderHTML += '<div class="form-group">';
    renderHTML += '<input id="search" type="text" name="query" class="form-control" placeholder="search here" onkeyup="updateSearchResults()"> &emsp;';
    renderHTML += '<div class="form-group"><select id="table" name="table" class="form-control" onchange="updateSearchResults()">';
    renderHTML += '<option value="Years">years</option><option value="Titles">titles</option><option value="Skits">skits</option></select></div>';
    renderHTML += '</div></form><br><br>';

    element.innerHTML = renderHTML;
    return updateSearchResults();
}

function displayYear(year) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Year';

    if (year === undefined) {
        year = ''
    }

    displayResults('/Years/' + year);
}

function displayTitle(title) {
    CurrentNotebook = 'Title';
    if (title === undefined) {
        title = ''
}
    displayResults('/Titles/' + title);
}

function displaySkit(skit) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Skit';
    if (skit === undefined) {
        skit = ''
}
    displayResults('/Skits/' + skit);
}
/* Render functions */

function renderGallery(data, prefix) {
    var element = document.getElementById('notebook-body-contents');
    var renderHTML = '';
    renderHTML += '<div class="row">';

    for (var i=0; i < data.length; i++) {
        var item = data[i];
        var id = item[0];
        var name = item[1];
        var image = item[2];
        renderHTML += '<div class="col-xs-3">';
        renderHTML += '<a href="#" onclick="display' + prefix + '(' + id + ')' + '" class="thumbnail text-center">';
        renderHTML += '<img src="' + image + '" width="300px">';
        renderHTML += '<div class="caption"><h4>' + name + '</h4></div>';
        renderHTML += '</a>';
        renderHTML += '</div>';
        if (i!=0 && i%4 == 0) {
            renderHTML += '</div>';
            renderHTML += '<div class = "row">';
        }
    }
renderHTML += '</div>';

element.innerHTML = renderHTML;
}

function renderTitle(data) {
    var element = document.getElementById('notebook-body-contents');
    var renderHTML = '';

    renderHTML += '<table class="table table-striped">';
    renderHTML += '<thead><th>Number</th><th>Name</th><th>Link</th></thead><tbody>';

    for (var i=0; i < data.length; i++) {
        var item = data[i];
        var id = item[0];
        var number = item[1];
        var name = item[2];
        var url = item[3]
        renderHTML += '<tr><td>' + number + '</td><td><a href="#" onclick="displaySkit(' + id + ')'  + '">' + name + '</a></td>';
        renderHTML += '<td><a href="' + url + '"><i class="fa fa-video-camera" aria-hidden="true"></i></a>';
        renderHTML += '</td></tr>';

    }
    renderHTML += '</tbody>';
    renderHTML += '</table>';

    element.innerHTML = renderHTML;
}

function renderSkit(data) {
    var element = document.getElementById('notebook-body-contents');
    var renderHTML = '';
    renderHTML += '<div class="row">';
    var id = data[0];
    var yearid = data[1];
    var titleimage = data[7];
    var year = data[2];
    var titlename = data[4];
    var number = data[5];
    var skitname = data[6];
    var titleid = data[3];
    var url = data[8];
    renderHTML += '<div class="col-sm-3">';
    renderHTML += '<a href="' + url + '"><img src="' + titleimage + '" width="300"></a>';
    renderHTML += '</div>';
    renderHTML += '<div class="col-sm-6"><dl><dt>Skit ID</dt><dd>' + id + '</dd><dt>Year</dt><dd><a href="#" onclick="displayYear(' + yearid + ')">' + year + '</a></dd>';
    renderHTML += '<dt>Title</dt><dd><a href="#" onclick="displayTitle(' + titleid + ')">' + titlename + '</a></dd>';
    renderHTML += '<dt>Skit Number</dt><dd>' + number + '</dd><dt>Skit Name</dt><dd><a href="' + url + '">' + skitname + '</a></dd></dl></div></div>';

    element.innerHTML = renderHTML;

}

/* Update functions */

function updateSearchResults() {
    var search = document.getElementById('search').value;
    var table = document.getElementById('table').value;
    renderHTML = '';
    renderHTML += '/search?query=' + search + '&table=' + table;

    displayResults(renderHTML);


}

function updateRevueInformation() {

    var skitName = document.getElementById('skitName');
    var titleImage = document.getElementById('titleImage');
    var yearName = document.getElementById('Year');
    var titleImage = document.getElementById('titleImage');

    skitName.innerHTML = revue['skitName'];
    titleImage.innerHTML = revue['titleImage'];
    year.innerHTML = revue['year'];
    titleImage.innerHTML = revue['titleImage'];
}

/* Registrations */
window.onload = function() {
    /* TODO: Register displayTab function for each tab's onclick callback */
    var tabs = document.getElementsByClassName('nav-tabs')[0].getElementsByTagName('li');
    for (i = 0; i < tabs.length; i++) {
        tabs[i].onclick = displayTab;
    }

    displaySearch();

};
