import tornado.ioloop
import tornado.options
import tornado.web

PORT = 1976
IMAGES = [
    'http://keenanhall.com/wp-content/uploads/2014/06/13723154453_689aa45006_k.jpg',
    'http://cdn.c.photoshelter.com/img-get/I000042rsiQWl2.Y/s/650/2-22-13-Keenan-Revue-4.jpg',
    'http://40.media.tumblr.com/7383dfff95a62b99449875e64517b460/tumblr_inline_nq28cb0I2s1s1comw_500.jpg'
    ]


class RevueHandler(tornado.web.RequestHandler):
    def get(self):
        name = self.get_argument('name', None)
        answer = self.get_argument('answer', None)
        self.render('Revue.html', name = name, answer = answer, photos = IMAGES)

class VideoHandler(tornado.web.RequestHandler):
   
    def get(self):
        name = self.get_argument('name', None)
        self.render('RevueVideo.html', name=name)

class HistoryHandler(tornado.web.RequestHandler):
   
    def get(self):
        name = self.get_argument('name', 'unknown')
        self.render('RevueHistory.html', photos = IMAGES)

        
Application = tornado.web.Application([
    (r"/", RevueHandler),
    (r"/videos/", VideoHandler),
    (r"/history/", HistoryHandler)
], debug=True)

Application.listen(PORT)

tornado.options.parse_command_line()
tornado.ioloop.IOLoop.current().start()
