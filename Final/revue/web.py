''' revue.web - Web Application '''

from revue.database import Database

import json
import logging
import socket
import socket

import tornado.ioloop
import tornado.web

# Application

class Application(tornado.web.Application):
    DEFAULT_PORT  = 9876
    TEMPLATE_PATH = 'assets/html'

    def __init__(self, port=DEFAULT_PORT):
	tornado.web.Application.__init__(self, debug=True, template_path=Application.TEMPLATE_PATH)
        self.logger   = logging.getLogger()
        self.database = Database()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        self.port     = port

        self.add_handlers('', [
            (r'/'           , IndexHandler),
            (r'/search'     , SearchHandler),
            (r'/Years/(.*)'  , YearHandler),
            (r'/Titles/(.*)' , TitleHandler),
            (r'/Skits/(.*)'  , SkitHandler),
            (r'/url/(.*)'   , URLHandler),
            (r'/assets/(.*)', tornado.web.StaticFileHandler, {'path': 'assets'}),
        ])

    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('revue.html')

class SearchHandler(tornado.web.RequestHandler):
    def get(self):
        query   = self.get_argument('query', '')
        table   = self.get_argument('table', '')
        results = []

        if table == 'Years':
            results = self.application.database.years(query)
        elif table == 'Titles':
            results = self.application.database.titles(query)
        elif table == 'Skits':
            results = self.application.database.skits(query)

        self.write(json.dumps({
            'render' : 'gallery',
            'prefix' : table[:-1],
            'results': list(results),
        }))

class YearHandler(tornado.web.RequestHandler):
    def get(self, year_id=None):
        if not year_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Year',
                'results': list(self.application.database.years('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Title',
                'results': list(self.application.database.year(year_id)),
            }))

class TitleHandler(tornado.web.RequestHandler):
    def get(self, title_id=None):
        if not title_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Title',
                'results': list(self.application.database.titles('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'title',
                'results': list(self.application.database.title(title_id)),
            }))

class SkitHandler(tornado.web.RequestHandler):
    def get(self, skit_id=None):
        if not skit_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'Skit',
                'results': list(self.application.database.skits('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'skit',
                'results': list(self.application.database.skit(skit_id)),
            }))

class URLHandler(tornado.web.RequestHandler):
    def get(self, skits_id=None):
        self.write(json.dumps({
            'URL': self.application.database.url(url_id),
        }))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
