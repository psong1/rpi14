''' revue.database - SQLite Database '''

import logging
import sqlite3
import yaml

# Database

class Database(object):
    SQLITE_PATH = 'revue.db'
    YAML_PATH   = 'assets/yaml/revue.yaml'

    def __init__(self, data=YAML_PATH, path=SQLITE_PATH):
        self.logger = logging.getLogger()
        self.data   = data
        self.path   = path
        self.conn   = sqlite3.connect(self.path)

        self._create_tables()
        self._load_tables()

    def _create_tables(self):
        years_sql = '''
        CREATE TABLE IF NOT EXISTS Year (
            YearID      INTEGER NOT NULL,
            Year        INTEGER NOT NULL,
            Image       TEXT NOT NULL,
            PRIMARY KEY (YearID)
        )
        '''

        titles_sql = '''
        CREATE TABLE IF NOT EXISTS Title (
            YearID      INTEGER NOT NULL,
            TitleID      INTEGER NOT NULL,
            Name        TEXT    NOT NULL,
            Image       TEXT    NOT NULL,
            PRIMARY KEY (TitleID),
            FOREIGN KEY (YearID) REFERENCES Year(YearID)
        )
        '''

        skit_sql = '''
        CREATE TABLE IF NOT EXISTS Skit (
            TitleID      INTEGER NOT NULL,
            SkitID      INTEGER NOT NULL,
            Number      INTEGER NOT NULL,
            Name        TEXT    NOT NULL,
            URL         TEXT    NOT NULL,
            PRIMARY KEY (SkitID),
            FOREIGN KEY (TitleID) REFERENCES Title(TitleID)
        )
        '''

        with self.conn:
            cursor = self.conn.cursor()
            cursor.execute(years_sql)
            cursor.execute(titles_sql)
            cursor.execute(skit_sql)

    def _load_tables(self):
        year_sql = 'INSERT OR REPLACE INTO Year (YearID, Year, Image) VALUES (?, ?, ?)'
        title_sql  = 'INSERT OR REPLACE INTO Title  (YearID, TitleID, Name, Image) VALUES (?, ?, ?, ?)'
        skit_sql  = 'INSERT OR REPLACE INTO Skit  (TitleID, SkitID, Number, Name, URL) VALUES (?, ?, ?, ?, ?)'

        with self.conn:
            cursor    = self.conn.cursor()
            year_id = 1
            title_id  = 1
            skit_id  = 1

            for year in yaml.load(open(self.data)):
                cursor.execute(year_sql, (year_id, year['year'], year['image']))
                self.logger.debug('Added Year: id={}, year={}'.format(year_id, year['year']))

                for title in year['titles']:
                    cursor.execute(title_sql, (year_id, title_id, title['name'], title['image']))
                    self.logger.debug('Added Title: id={}, name={}'.format(title_id, title['name']))

                    for skit_number, skit_object in enumerate(title['skits']):
                        cursor.execute(skit_sql, (title_id, skit_id, skit_number + 1, skit_object['name'], skit_object['url']))
                        self.logger.debug('Added Skit: id={}, name={}'.format(skit_id, skit_object['name']))
                        skit_id += 1

                    title_id += 1

                year_id += 1

    def years(self, year):
        years_sql = 'SELECT YearID, Year, Image FROM Year WHERE Year LIKE ? ORDER BY Year'
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(years_sql, ('%{}%'.format(year),))

    def year(self, year_id=None):
        year_sql = '''
        SELECT      TitleID, Name, Image
        FROM        Title
        WHERE       YearID = ?
        ORDER BY    Name
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(year_sql, (year_id,))

    def titles(self, name):
        titles_sql = 'SELECT TitleID, Name, Image FROM Title WHERE Name LIKE ? ORDER BY Name'
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(titles_sql, ('%{}%'.format(name),))

    def title(self, title_id=None):
        title_sql = '''
        SELECT      SkitID, Number, Name, Skit.URL
        FROM        Skit
        WHERE       TitleID = ?
        ORDER BY    Number
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(title_sql, (title_id,))

    def skits(self, skit):
        skits_sql = '''
        SELECT      Skit.SkitID, Skit.Name, Image, Skit.URL
        FROM        Skit
        JOIN        Title
        ON          Skit.TitleID = Title.TitleID
        WHERE       Skit.Name LIKE ?
        ORDER BY    Skit.Name
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(skits_sql, ('%{}%'.format(skit),))

    def skit(self, skit_id=None):
        skit_sql = '''
        SELECT      SkitID, Year.YearID, Year.Year, Title.TitleID, Title.Name, Skit.Number, Skit.Name, Title.Image, Skit.URL
        FROM        Skit
        JOIN        Title
        ON          Title.TitleID = Skit.TitleID
        JOIN        Year
        ON          Year.YearID = Title.TitleID
        WHERE       SkitID = ?
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(skit_sql, (skit_id,)).fetchone()

def url(self, skit_id=None):
    url_sql = '''
    SELECT      Skit.Name, Year.Year, Title.Name, Title.Image, Skit.Number, Skit.URL
    FROM        Skit
    JOIN        Title
    ON          Title.TitleID = Skit.TitleID
    JOIN        Year
    ON          Year.YearID = Title.YearID
    WHERE       SkitID LIKE ?
    '''
    with self.conn:
        cursor = self.conn.cursor()
        urls = cursor.execute(url_sql, (skit_id,)).fetchone()
        data = {
            'skitName'   : urls[0],
            'yearYear'   : urls[1],
            'titleName'  : urls[2],
            'titleImage' : urls[3],
            'skitNumber' : urls[4],
            'skitURL'    : urls[5],
        }
        return data

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
