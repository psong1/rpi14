Projects
========

This is the Projects repository for [CDT-30020-SP16].

Group
-----

The members of this group are:

- Carly Gundy (cgundy@nd.edu)
- Andrew Russell (arussel5@nd.edu)
- Patricia Song (psong1@nd.edu)

[CDT-30020-SP16]: https://www3.nd.edu/~pbui/teaching/cdt.30020.sp16/