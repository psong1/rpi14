''' searchit.web - Web Application '''

from searchit.database import Database

import logging

import tornado.ioloop
import tornado.web

# Application

class Application(tornado.web.Application):
    DEFAULT_PORT  = 9876
    TEMPLATE_PATH = 'assets/html'

    def __init__(self, port=DEFAULT_PORT):
	tornado.web.Application.__init__(self, debug=True, template_path=Application.TEMPLATE_PATH)
        # TODO: Initialize database and port
        self.logger   = logging.getLogger()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        self.database = Database()
        self.port     = port

        # TODO: Add Index, Album, Artist, and Track Handlers
        self.add_handlers('', [
            (r'/'          , IndexHandler),
            (r'/album/(.*)', AlbumHandler),
            (r'/artist/(.*)', ArtistHandler),
            (r'/track/(.*)', TrackHandler),
        ])

    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        # TODO: Implement Index Handler
        query = self.get_argument('query', '')
        table = self.get_argument('table', '')
        if (table =='Artists'):
            artists = self.application.database.artists('{}'.format(query))
            self.render('gallery.html', query=query, table=table, data=artists, prefix = 'artist')
        elif (table == 'Albums'):
            albums = self.application.database.albums('{}'.format(query))
            self.render('gallery.html', query=query, table=table, data=albums, prefix = 'album')
        elif (table == 'Tracks'):
            tracks = self.application.database.tracks('{}'.format(query))
            self.render('gallery.html', query=query, table=table, data=tracks, prefix = 'track')
        else:
            self.render('index.html', query=query, table=table)
        

class ArtistHandler(tornado.web.RequestHandler):
    def get(self, artist_id=None):
        # TODO: Implement Artist Handler
        if artist_id:
            artists = self.application.database.artist(artist_id)
            self.render('gallery.html', query='', table='', data=artists, prefix = 'album')
            
        else:
            gallery = self.application.database.artists( '' )
            self.render('gallery.html', query='', data = gallery, prefix = 'artist')
        
class AlbumHandler(tornado.web.RequestHandler):
    def get(self, album_id=None):
        # TODO: Implement Album Handler
        if album_id:
            albums = self.application.database.album(album_id)
            self.render('album.html', query='', table='', data=albums)
            
        else:
            gallery = self.application.database.albums( '' )
            self.render('gallery.html', query='', data = gallery, prefix = 'album')
         
        
class TrackHandler(tornado.web.RequestHandler):
    def get(self, track_id=None):
        # TODO: Implement Album Handler
        if track_id:
            tracks = self.application.database.track(track_id)
            self.render('track.html', query='', table='', data=tracks)
            
        else:
            gallery = self.application.database.tracks( '' )
            self.render('gallery.html', query='', data = gallery, prefix = 'track')
        
        
# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
