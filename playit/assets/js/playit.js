/* Globals */

var PlaylistQueue   = [];	    /* List of songs	*/
var PlaylistIndex   = -1;	    /* Current song	*/
var CurrentNotebook = 'Search';	    /* Current notebook */

/* AJAX functions */

function requestJSON(url, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
    	if (request.readyState == 4 && request.status == 200) {
    	    callback(JSON.parse(request.responseText));
	}
    }

    request.open('GET', url, true);
    request.send();
}

/* Display functions */

function displayTab() {
    /* TODO: Remove the active class from each notebook tab */
    var tabs = document.getElementsByClassName('nav-tabs')[0].getElementsByTagName('li');
    for (var i = 0; i < tabs.length; i++) {
        var tab = tabs[i];
        tab.classList.remove('active');
    }
    this.classList.add('active');
    var tabs = this.getElementsByTagName('a')[0].innerHTML;
    switch (tabs) {
        case 'search':
            displaySearch();
            break;
        case 'artists':
            displayArtist();
            break;
        case 'albums':
            displayAlbum();
            break;
        case 'tracks':
            displayTrack();
            break;
        case 'playlist':
            displayPlaylist();
            break;
    }
}

function displayResults(url) {
    /* TODO: Request JSON from URL and then call appropriate render function */
    requestJSON(url, function(data) {
        if (data['render'] == 'gallery') {
        renderGallery(data['results'], data['prefix']);
    } else if (data['render'] == 'album') {
        renderAlbum(data['results']);
    } else if (data['render'] == 'track') {
        renderTrack(data['results']);
    }
});
}

function displaySearch(query) {
    CurrentNotebook = 'Search';

    var element   = document.getElementById('notebook-body-nav');
    var renderHTML = '<form class="navbar-form text-center">';

    renderHTML += '<div class="form-group">';
    renderHTML += '<input id="search" type="text" class="form-control" name="query" placeholder="search here!" onkeyup="updateSearchResults()">';
    renderHTML += '<div class="form-group"><select id="table" name="table" class="form-control" onchange="updateSearchResults()">';
    renderHTML += '<option value="Artists">Artists</option><option value="Albums">Albums</option><option value="Tracks">Tracks</option></select></div>';
    renderHTML += '</div></form>';

    element.innerHTML = renderHTML;
    return updateSearchResults();
}

function displayArtist(artist) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Artists';

    if (artist === undefined) {
        artist = ''
    }

    displayResults('/Artists/' + artist);
}

function displayAlbum(album) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Albums';
    if (album === undefined) {
        album = ''
}
    displayResults('/Albums/' + album);
}

function displayTrack(track) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Tracks';
    if (track === undefined) {
        track = ''
}
    displayResults('/Tracks/' + track);
}

function displayPlaylist() {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = 'Playlist';

    var element    = document.getElementById('notebook-body-contents');
    renderHTML = '<table id = "playlist" class="table table-striped table-bordered text-center">';
    renderHTML += '<thead><th>#</th><th>Album Cover</th><th>Artist</th><th>Album</th><th>Track</th></thead>';

    var table = document.getElementById('playlist');

    for (var i=0; i < PlaylistQueue.length; i++) {
        var items = PlaylistQueue[i]
        renderHTML += '<tr>';
        renderHTML += '<td>' + (i+1) + '</td> <td>' + '<img width="50" src="' + item['albumImage'] + '">' + ",/td> <td>" + item['artistName'] + "</td> <td>" + item['albumName'] + "</td> <td>" + item['trackName'] + '</td>';
        renderHTML += '</tr>';
    }

    element.innerHTML = renderHTML;
}
/* Render functions */

function renderGallery(data, prefix) {
    var element = document.getElementById('notebook-body-contents');
    var renderHTML = '';
    renderHTML += '<div class="row">';

    for (var i=0; i < data.length; i++) {
        var item = data[i];
        var id = item[0];
        var name = item[1];
        var image = item[2];
        renderHTML += '<div class="col-xs-3">';
        renderHTML += '<a href="#" onclick="display' + prefix + '(' + id + ')' + '"" class="thumbnail text-center">';
        renderHTML += '<img src="' + image + '">';
        renderHTML += '<div class="caption"><h4>' + name + '</h4></div>';
        renderHTML += '</a>';
        renderHTML += '</div>';
        if (i!=0 && i%4 == 0) {
            renderHTML += '</div>';
            renderHTML += '<div class = "row">';
        }
    }
renderHTML += '</div>';

element.innerHTML = renderHTML;
}

function renderAlbum(data) {
    var element = document.getElementById('notebook-body-contents');
    var renderHTML = '';
    renderHTML += '<table class="table table-striped">';
    renderHTML += '<thead><th>Number</th><th>Name</th><th>Actions</th></thead><tbody>';

    for (var i=0; i < data.length; i++) {
        var item = data[i];
        var id = item[0];
        var number = item[1];
        var trackname = item[2];
        renderHTML += '<tr><td>' + number + '</td><td><a href="#" onclick="displayTrack(' + id + ')'  + '">' + trackname + '</a></td>';
        renderHTML += '<td><a href="#" onclick="addSong(' + id + ')"><i class="fa fa-plus"></i></a> &emsp;';
        renderHTML += '<a href="#" onclick="playSong(' + id + ')' + '">' + '<i class="fa fa-play"></i></a>';
        renderHTML += '</td></tr>';

    }
    renderHTML += '</tbody>';
    renderHTML += '</table>';

    element.innerHTML = renderHTML;
}

function renderTrack(data) {
    var element = document.getElementById('notebook-body-contents');
    var renderHTML = '';
    renderHTML += '<div class="row">';
    var id = data[0];
    var artistid = data[1];
    var albumimage = data[7];
    var artistname = data[2];
    var albumname = data[4];
    var number = data[5];
    var trackname = data[6];
    var albumid = data[3];
    renderHTML += '<div class="col-sm-3">';
    renderHTML += '<a href="#" onclick="playSong(' + id + ')"' + ' class="thumbnail text-center"><img src="' + albumimage + '"></a>';
    renderHTML += '</div>';
    renderHTML += '<div class="col-sm-6"><dl><dt>Track ID</dt><dd>' + id + '</dd><dt>Artist</dt><dd><a href="#" onclick="displayArtist(' + artistid + ')">' + artistname + '</a></dd>';
    renderHTML += '<dt>Album</dt><dd><a href="#" onclick="displayAlbum(' + albumid + ')">' + albumname + '</a></dd>';
    renderHTML += '<dt>Track Number</dt><dd>' + number + '</dd><dt>Track Name</dt><dd>' + trackname + '</dd></dl></div></div>';

    element.innerHTML = renderHTML;

}

/* Update functions */

function updateSearchResults() {
    var search = document.getElementById('search').value;
    var table = document.getElementById('table').value;
    renderHTML = '';
    renderHTML += '/search?query=' + search + '&table=' + table;

    displayResults(renderHTML);


}

function updateSongInformation() {

    var trackName = document.getElementById('trackName');
    var albumImage = document.getElementById('albumImage');
    var artistName = document.getElementById('artistName');
    var ablumImage = document.getElementById('albumImage');
    var player = document.getElementById('player');

    trackName.innerHTML = song['trackName'];
    albumImage.innerHTML = song['albumImage'];
    artistName.innerHTML = song['artistName'];
    albumImage.innerHTML = song['albumImage'];
    player.innerHTML = song['player'];

    if (CurrentNotebook == 'Playlist') {
        displayPlaylist();
    }
}

/* Audio controls */

function addSong(trackId, select) {
    var url = "/song/" + trackId;

    requestJSON(url, function(data) {
	var song = data['song'];
	PlaylistQueue.push(song);

	if (select !== undefined && select) {
	    selectSong(PlaylistIndex + 1);
	}
    });
}

function prevSong() {
    if (PlaylistIndex < 0 || PlaylistQueue.length == 0) {
    	return;
    }

    selectSong((PlaylistIndex + PlaylistQueue.length - 1) % PlaylistQueue.length);
}

function nextSong() {
    if (PlaylistIndex < 0 || PlaylistQueue.length == 0) {
    	return;
    }

    selectSong((PlaylistIndex + 1) % PlaylistQueue.length);
}

function selectSong(index) {
    PlaylistIndex = index;
    updateSongInformation();
    playSong();
}

function playSong(trackId) {
    var player     = document.getElementById('player');
    var playButton = document.getElementById('playButton');

    if (trackId !== undefined && trackId != '') {
    	return addSong(trackId, true);
    }

    if (PlaylistIndex < 0 && PlaylistQueue.length) {
    	return selectSong(0);
    }

    if (player.src === undefined || player.src == '') {
    	return;
    }

    if (player.ended || player.paused) {
    	playButton.innerHTML = '<i class="fa fa-pause"></i>';
	player.play();
    } else {
    	playButton.innerHTML = '<i class="fa fa-play"></i>';
	player.pause();
    }
}

function togglePlaySong() {
    playSong('');
}

function endSong() {
    document.getElementById('playButton').innerHTML = '<i class="fa fa-play"></i>';
    nextSong();
}

/* Registrations */
window.onload = function() {
    /* TODO: Register displayTab function for each tab's onclick callback */
    var tabs = document.getElementsByClassName('nav-tabs')[0].getElementsByTagName('li');
    for (i = 0; i < tabs.length; i++) {
        tabs[i].onclick = displayTab;
    }

    document.getElementById('prevButton').onclick = prevSong;
    document.getElementById('playButton').onclick = togglePlaySong;
    document.getElementById('nextButton').onclick = nextSong;

    displaySearch();

};
